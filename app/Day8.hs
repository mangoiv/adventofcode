module Day8 ( solution1
            , getInputs
            , buildDigits
            , showDigits
            ) where

import           Data.Function (fix)
import           Data.List     (partition)
import           Data.Map      (Map, elems, insert, insertWith, keys, member,
                                update)
import qualified Data.Map      as M
import           Data.Maybe    (mapMaybe)
import           Data.Set      (Set, fromList, intersection, isSubsetOf, size,
                                (\\))
import qualified Data.Set      as S
import           Debug.Trace

data Bar = A | B | C | D | E | F | G
  deriving (Eq, Ord, Enum, Bounded, Show)

type Digit = [Bar]

type Output = [Digit]

type Possibilities = Set Char

type Assoc = Map Bar Possibilities

-- first care about numbers that have a unique number of bars
-- save possible configurations and then find the only possible one
--

showDigits :: Output -> Maybe [Int]
showDigits = mapM showDigit
  where
    showDigit :: Digit -> Maybe Int
    showDigit d
      | isSubsetOf (fromList d) (fromList one) = Just 1
      | isSubsetOf (fromList d) (fromList two) = Just 2
      | isSubsetOf (fromList d) (fromList seven) = trace ("\nd is"++ show d) $ Just 7
      | isSubsetOf (fromList d) (fromList three) = trace ("\nd is"++ show d) $ Just 3
      | isSubsetOf (fromList d) (fromList four) = Just 4
      | isSubsetOf (fromList d) (fromList five) = Just 5
      | isSubsetOf (fromList d) (fromList six) = Just 6
      | isSubsetOf (fromList d) (fromList nine) = Just 9
      | isSubsetOf (fromList d) (fromList zero) = Just 0
      | isSubsetOf (fromList d) (fromList eight) = Just 8
      | otherwise = trace ("\nd is " ++ show d) Nothing
-- this is ugly af

buildDigits :: ([String],[String]) -> Output
buildDigits (garble,out) = flip buildDigit model <$> out
  where
    model :: Assoc
    model =  reduce $ buildAssoc garble M.empty

buildDigit :: String -> Assoc -> Digit
buildDigit s = keys . M.filter (flip isSubsetOf $ fromList s)
-- this probably needs to be refined

buildAssoc :: [String] -> Assoc -> Assoc
buildAssoc s a = let res = foldl (flip testForDetNumbers) a s
                  in if res == a
                        then res
                        else buildAssoc s res

testForDetNumbers :: String -> Assoc -> Assoc
testForDetNumbers s = sN eight . sN seven . sN four . sN one
  where
    sN :: [Bar] -> Assoc -> Assoc
    sN = num s

reduce :: Assoc -> Assoc
reduce m = let res = M.map (`reduceForSet` m) m
            in if res == m
                  then res
                  else reduce res
  where
    reduceForSet :: Possibilities -> Assoc -> Possibilities
    reduceForSet s m = foldl (\acc x -> if 1 <= size (acc \\ x) then acc \\ x else acc) s m



-- definite solution
one   = [C,F]
seven = [A,C,F]
four  = [B,C,D,F]
eight = [A,B,C,D,E,F,G]
-- no deterministic solution
zero  = [A,B,C,E,F,G]
two   = [A,C,D,E,G]
three = [A,C,D,F,G]
five  = [A,B,D,F,G]
six   = [A,B,D,E,F,G]
nine  = [A,B,C,D,F,G]




-- keys::[Bar] -> The Bars in the segment associated with the number
-- m::Assoc -> The initial association map
-- s::String -> the String that contains the possible chars for the bars
-- sample call:
-- let one = [C,F] in
-- num one (M.fromList [(C,fromList "gf")]) "ag"
num :: String -> [Bar] -> Assoc -> Assoc
num s keys m = let (inM, new) = partition (`member` m) keys in if length s == length keys then newKeys new $ modMap inM m else m
  where
    chrs :: Set Char
    chrs = fromList s
    newKeys :: [Bar] -> Assoc -> Assoc
    newKeys bs mp = foldl (\acc x -> insert x chrs acc) mp bs
    modMap :: [Bar] -> Assoc -> Assoc
    modMap bs mp =  foldl (flip (update reduceSet)) mp bs
    reduceSet :: Possibilities -> Maybe Possibilities
    reduceSet ps = Just $ intersection ps chrs

-- the problem
-- the map isfromList [(A,fromList "cdf"),(B,fromList "cdf"),(C,fromList "cdf"),(D,fromList "cdf"),(E,fromList "cdf"),(F,fromList "cdf"),(G,fromList "cdf")]
-- thee map (inner) is fromList [(A,fromList "cdf"),(B,fromList "cdf"),(C,fromList "cdf"),(D,fromList "cdf"),(E,fromList "cdf"),(F,fromList "cdf"),(G,fromList "cdf")
getDigit :: String -> Maybe Assoc
getDigit = undefined




solution1 :: [([String], [String])] -> Int
solution1 =  length . ((mapMaybe getDig . snd) =<<)
  where
    getDig :: String -> Maybe Int
    getDig t = case length t of
                 2 -> Just 1
                 4 -> Just 4
                 3 -> Just 7
                 7 -> Just 8
                 _ -> Nothing

-- should be specialized as soon as the next task allows it
getInputs :: String -> ([String], [String])
getInputs = (,) <$> words . fst . spBar <*> words . snd . spBar
  where
    spBar :: String -> (String, String)
    spBar = fmap tail . span (/='|')
