import std/strutils
import std/algorithm
import std/sequtils
import std/sugar
import std/sets
import std/math

proc printMatrix[T](a: openArray[T]):void =
  for row in a:
    echo(row)
  echo("\n\n")

func isLowest(i: int, s: openarray[int]): bool =
  if i < 1:
    if s[i] < s[i+1]:
      return true
    else:
      return false
  elif i == s.len-1:
    if s[i] < s[i-1]:
      return true
    else:
      return false
  else:
    if s[i] < s[i-1] and s[i]<s[i+1]:
      return true
    else:
      return false


const 
  input = splitLines(readFile("../assets/input9"))
  rows = input.len - 1
  cols = input[0].len

var 
  matrix: array[rows,array[cols, int]]
  colMatrix: array[cols,array[rows, int]]
  lowPointsRows: array[rows,array[cols, bool]]
  lowPointsCols: array[rows,array[cols, bool]]
  lowPoints: array[rows,array[cols, bool]]
  riskLevels: int


# get the height map
for i in 0 ..< rows:
  for j in 0 ..< cols:
    matrix[i][j] = parseInt($input[i][j])

# get the lowpoints within rows
for i in 0 ..< rows:
  for j in 0 ..< cols:
    lowPointsRows[i][j] = isLowest(j, matrix[i])

# build the transposed matrix
for row in 0 ..< cols: 
  var col: array[rows,int]
  for e in 0 ..< rows:
    col[e] = matrix[e][row]
  colMatrix[row] = col


# get the lowpoints within cols
for i in 0 ..< rows:
  for j in 0 ..< cols:
    lowPointsCols[i][j] = isLowest(i, colMatrix[j])

# look where in the matrices the bit is equal
for i in 0 ..< rows: 
  for j in 0 ..< cols:
    if lowPointsRows[i][j] and lowPointsCols[i][j]:
      lowPoints[i][j] = true
    else:
      lowPoints[i][j] = false

# get the risk levels for the matrix
for i in 0 ..< rows: 
  for j in 0 ..< cols:
    if lowPoints[i][j]:
      riskLevels += matrix[i][j] + 1

echo "The sum of the risklevels is " & $riskLevels

# task 2
# ======

type Coord = tuple 
  x:int 
  y:int

var
  nines: array[rows,array[cols, int]]
  areas: seq[seq[Coord]]

proc neighbours(c0: Coord, c1: Coord): bool =
  let 
    (x0,y0) = c0
    (x1,y1) = c1
    d0 = abs(x1-x0)
    d1 = abs(y1-y0)
  if d0 <= 1 and d1 <= 1 and d0+d1 < 2:
    return true
  else:
    return false

proc anyTwoEqual[T](s0: seq[T], s1: seq[T]): bool = 
  var
    set0 = toHashSet s0
    set1 = toHashSet s1
  return (set0+set1).len < (s0.len + s1.len)

for i in 0 ..< rows:
  for j in 0 ..< cols:
    if matrix[i][j] == 9: 
      nines[i][j] = 0
    else: 
      nines[i][j] = 1

for i in 0 ..< rows:
  for j in 0 ..< cols:
    if matrix[i][j] != 9: 
      var lone = true
      for basin in areas.mitems:
        if basin.any(c => neighbours(c,(i,j))):
          basin.add((i,j))
          lone = false
      if lone: 
        areas.add(@[(i,j)]) 

for b1 in areas.mitems:
  for b2 in areas:
    if anyTwoEqual(b1,b2): 
      b1 = toSeq((toHashSet b2)+(toHashSet b1)) 
      
let aSet: seq[seq[Coord]] = toSeq(toHashSet(areas.map(basin => toSeq(toHashSet (basin))))).sortedByIt(it.len).reversed

echo "product is " & $prod(aSet[0..2].map(basin => basin.len))
