{-# LANGUAGE InstanceSigs #-}
module Day5 ( Line (..)
            , mkLine
            , solution1
            , solution2
            ) where

import           Control.Applicative
import           Control.Monad
import           Data.Char
import qualified Data.Map            as M
import           Data.Maybe

-- today is about understanding writing a Parser from scratch

newtype Parser a = Parser { parse :: String -> [(a, String)] }

instance Monad Parser where
  return a = Parser $ \s -> [(a,s)]

  p >>= f = Parser $ (\(a', s') -> parse (f a') s') <=< parse p

instance Applicative Parser where
  pure = return

  liftA2 f (Parser p1) (Parser p2) = Parser $
    \s ->  [(f r1 r2, sf) |
      (r1, si) <- p1 s  ,
      (r2, sf) <- p2 si ]

instance Functor Parser where
  fmap :: (a->b) -> Parser a -> Parser b
  fmap f (Parser p) = Parser $ \s -> [(f v, sf) | (v, sf) <- p s ]

instance Alternative Parser where
  empty = Parser $ const []
  Parser p1 <|> Parser p2 = Parser $ \s ->
    case p1 s of
      []  -> p2 s
      res -> res

instance MonadPlus Parser where
  mzero = empty
  mplus (Parser p1) (Parser p2) = Parser $ \s -> p1 s ++ p2 s


runParser :: Parser a -> String -> Maybe a
runParser m s =
  case parse m s of
    [(res, [])] -> Just res
    _           -> Nothing

item :: Parser Char
item = Parser $
  \case
    []     -> []
    (c:cs) -> [(c, cs)]


sat :: (Char -> Bool) -> Parser Char
sat f = do
  n <- item
  if f n then return n else empty

digit :: Parser Char
digit = sat isDigit

number :: Parser Int
number = do
  ds <- many digit
  return $ read ds

string :: String -> Parser String
string [] = return []
string s@(c:cs) = do
  sat (==c)
  string cs
  return s

sep :: Parser String
sep = string " -> "

comma :: Parser String
comma = string ","

done :: Parser String
done = many item

pair :: Parser (Int,Int)
pair = do
  x <- number
  comma
  y <- number
  return (x,y)

line :: Parser Line
line = do
  p1 <- pair
  sep
  p2 <- pair
  done
  let ln = Line p1 p2
  return ln

mkLine :: String -> Line
mkLine s = case runParser line s of
             Nothing -> error "Parser didn't succeed"
             Just a  -> a

-- actual task
data Line = Line { start :: (Int, Int)
                 , end   :: (Int, Int)
                 }
                 deriving Show

type Field = M.Map (Int,Int) Int

data Orientation = Vertical
                 | Horizontal
                 | DiagonalTB -- Diagonal, bottom -> top
                 | DiagonalBT -- Diagonal, top -> bottom

type OrientedLine = (Orientation, Line)

solution1 = solution False

solution2 = solution True

solution :: Bool -> [Line] -> Int
solution isWithDiagonal = length . M.filter (>1) . foldl lineOnField M.empty . orientedLines isWithDiagonal

-- |This functions is unsafe on lines that haven't been checked to be either horizontal or vertical
getCoords :: OrientedLine -> [(Int,Int)]
getCoords (o, Line {..}) =
  case o of
    Vertical   -> [(fst start,y) | y <- span (snd start) (snd end) ]
    Horizontal -> [(x, snd start) | x <- span (fst start) (fst end)]
    DiagonalTB -> scanl (\(x,y) b -> (x+b,y+b)) mi $ replicate diff 1
    DiagonalBT -> scanl (\(x,y) b -> (x+b,y-b)) mi $ replicate diff 1
  where
    span :: Int -> Int -> [Int]
    span x y = case compare x y of
                 LT -> [x..y]
                 GT -> [y..x]
                 EQ -> [x]
    mi = min start end
    ma = max start end
    diff = fst ma - fst mi

orientedLines :: Bool -> [Line] -> [OrientedLine]
orientedLines isWithDiagonal = mapMaybe ol
  where
    ol :: Line -> Maybe OrientedLine
    ol l@(Line start@(x1,y1) end@(x2,y2))
      | x1 == x2 = Just (Vertical, l)
      | y1 == y2 = Just (Horizontal, l)
      | isWithDiagonal && diffequal = if snd mi > snd ma
                                       then Just (DiagonalBT, l)
                                       else Just (DiagonalTB, l)
      | otherwise = Nothing
      where
        diffequal = abs (x1 - x2) == abs (y1 - y2)
        mi = min start end
        ma = max start end


lineOnField :: Field -> OrientedLine -> Field
lineOnField m l = let coords = getCoords l in foldl (\a c -> M.insertWith (+) c 1 a) m coords



