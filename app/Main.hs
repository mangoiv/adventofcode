module Main where

import qualified Day1
import qualified Day10
import qualified Day2
import qualified Day3
import qualified Day4
import qualified Day5
import qualified Day6
import qualified Day7
import qualified Day8

import qualified Data.Text    as T
import qualified Data.Text.IO as TIO

getInputs :: (String -> a) -> String -> IO [a]
getInputs p s = do
  fileContents <- readFile $ "assets/" ++ s
  return (p <$> lines fileContents)

getInput1 :: IO [Int]
getInput1 = getInputs read "input1"

getInput2 :: IO [Day2.Command Int]
getInput2 = getInputs Day2.toCommand "input2"

getInput3 :: IO [[Int]]
getInput3 = getInputs Day3.toBinary "input3"

getInput4 :: IO Day4.Game
getInput4 = do
  fileContents <- TIO.readFile "assets/input4"
  let ls = T.lines fileContents
  return (Day4.toGame ls)

getInput5 :: IO [Day5.Line]
getInput5 = getInputs Day5.mkLine "input5"

getInput6 :: IO [Int]
getInput6 = head <$> getInputs Day6.getFishies "input6"

getInput7 :: IO [Int]
getInput7 = head <$> getInputs Day6.getFishies "input7"

getInput8 :: IO [([String],[String])]
getInput8 = getInputs Day8.getInputs "input8"

getInput10 :: IO [String]
getInput10 = getInputs id "input10"

main :: IO ()
main = do
  putStrLn "Solutions, Day 1: "
  s1 <- Day1.solution <$> getInput1
  s2 <- Day1.solution . Day1.threeSums <$> getInput1
  print s1
  print s2

  putStrLn "Solutions, Day 2: "
  s3 <- Day2.solution Day2.dive1 <$> getInput2
  s4 <- Day2.solution Day2.dive2 <$> getInput2
  print s3
  print s4

  putStrLn "Solutions, Day 3: "
  s5 <- Day3.solution1 <$> getInput3
  s6 <- Day3.solution2 <$> getInput3
  print s5
  print s6

  putStrLn "Solutions, Day 4: "
  s7 <- Day4.solution1 <$> getInput4
  s8 <- Day4.solution2 <$> getInput4
  print s7
  print s8

  putStrLn "Solutions, Day 5: "
  s9 <- Day5.solution1 <$> getInput5
  s10 <- Day5.solution2 <$> getInput5
  print s9
  print s10

  putStrLn "Solutions, Day6: "
  s11 <- Day6.solution1 <$> getInput6
  s12 <- Day6.solution2 <$> getInput6
  print s11
  print s12

  putStrLn "Solutions, Day7: "
  s13 <- Day7.solution1 <$> getInput7
  s14 <- Day7.solution2 <$> getInput7
  print s13
  -- print s14

  putStrLn "Solutions, Day8: "
  s15 <- Day8.solution1 <$> getInput8
  print s15

  putStrLn "Solutions, Day10: "
  s19 <- Day10.solution1 <$> getInput10
  s20 <- Day10.solution2 <$> getInput10
  print s19
  print s20
