module Day7 ( solution1
            , solution2
            ) where

type Crab = Int
type Pos = Int
type Cost = Int

type CostFunction = Pos -> Crab -> Cost

solution1 :: [Crab] -> Int
solution1 = solution costToPos1

solution2 :: [Crab] -> Int
solution2 = solution costToPos2

solution :: CostFunction -> [Crab] -> Int
solution costToPos cs = minimum $ costForAllCrabs cs <$> [minimum cs..maximum cs]
  where
    allPositions :: [Crab] -> [Pos]
    allPositions cs = [minimum cs..maximum cs]

    costForAllCrabs :: [Crab] -> Pos -> Cost
    costForAllCrabs cs x = sum $ costToPos x <$> cs

costToPos1 :: CostFunction
costToPos1 pos crab = abs $ crab - pos

costToPos2 :: CostFunction
costToPos2 pos = multiplicator . costToPos1 pos

multiplicator :: Int -> Int
multiplicator = sum . flip take [1..]
