module Day2 ( Command (..)
            , toCommand
            , solution
            , dive1
            , dive2
            ) where
import           Data.Bits (Bits (xor))

data Command a = Forward a
               | Down a
               | Up a
               deriving (Show)

toCommand :: Read a => String -> Command a
toCommand s = let (com, rem) = span (/=' ') s
               in case com of
                "forward" -> Forward (read rem)
                "up"      -> Up (read rem)
                "down"    -> Down (read rem)
                _         -> error "This is not a valid command"


solution :: Num a => ((a,a,a) -> Command a -> (a,a,a)) -> [Command a] -> a
solution dive cs = let (_, dep, ver) = foldl dive (0,0,0) cs
               in dep * ver

dive1 :: Num a => (a, a, a) -> Command a -> (a, a, a)
dive1 (_, d, v) command =
  case command of
    Forward x -> ( undefined, d  , v+x )
    Up x      -> ( undefined, d-x, v   )
    Down x    -> ( undefined, d+x, v   )

dive2 :: Num a => (a,a,a) -> Command a -> (a,a,a)
dive2 (a,d,v) command =
  case command of
    Forward x -> ( a  , d+(a*x), v+x )
    Up x      -> ( a-x, d      , v   )
    Down x    -> ( a+x, d      , v   )

