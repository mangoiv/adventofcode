{-# LANGUAGE RecordWildCards #-}
module Day4 ( Game (..)
            , toGame
            , solution1
            , solution2
            ) where

import           Data.List
import qualified Data.Text as T

data Game = Game { numbers :: [Int]
                 , boards  :: [Board]
                 }
                 deriving Show

type Board = [[(Int, Marking)]]

data Marking = U | M
  deriving (Show, Eq)

solution1 :: Game -> Int
solution1 = solution . head . winners

solution2 :: Game -> Int
solution2 = solution . last . winners

-- |calculate the solution based on the winning function
solution :: ([Board],Int) -> Int
solution g = let (w, i) = g
              in case w of
                   [x] -> i * sum (fst <$> filter (\(_ , m) -> m == U) (concat x))
                   _   -> error "Too many winners"

winners :: Game -> [([Board], Int)]
winners Game {..} = go numbers boards
  where
    go :: [Int] -> [Board] -> [([Board], Int)]
    go [] _ = error "no winner for this Bingo game"
    go _ [] = []
    go (x:xs) bs = let
                      cbs = mark x <$> bs
                      wbs = filter check cbs
                    in (if not (null wbs) then ((wbs,x):) else id) (go xs (cbs\\wbs))

-- |Check whether either a line or a column is complete
check :: Board -> Bool
check b = or $ fmap complete [transpose b, b]
  where
    complete :: [[(a,Marking)]] -> Bool
    complete l = or $ all (\(_,m) -> m == M) <$> l


-- |Mark all Cells on the board that are equal to n
mark :: Int -> Board -> Board
mark n b = fmap m <$> b
  where
    m :: (Int, Marking) -> (Int,Marking)
    m (x,U) = if n == x then (x,M) else (x,U)
    m t     = t

-- |parse input to Game
toGame :: [T.Text] -> Game
toGame [] = error "empty Board"
toGame (l:ls) = Game { numbers = asNumbers l
                     , boards = asBoards }
                       where
                         asNumbers :: T.Text -> [Int]
                         asNumbers t = tread  <$> T.split (==',') t
                         asBoards :: [Board]
                         asBoards = reverse $ bds . reverse <$> bls [] ls

                         bls :: [[T.Text]] -> [T.Text] -> [[T.Text]]
                         bls acc []         = acc
                         bls acc ("":ls)    = bls ([]:acc) ls
                         bls [] (l:ls)      = bls [[l]] ls
                         bls (a:acc) (l:ls) = bls ((l:a):acc) ls

                         bds :: [T.Text] -> Board
                         bds l = wAsInt <$> l

                         wAsInt :: T.Text -> [(Int, Marking)]
                         wAsInt r = zip (tread <$> T.words r) (repeat U)

                         tread :: Read a => T.Text -> a
                         tread = read . T.unpack
