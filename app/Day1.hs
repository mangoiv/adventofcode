module Day1 ( solution
            , threeSums
            ) where

solution :: [Int] -> Int
solution l = length $ filter id $ zipWith (>) (tail l) l

threeSums :: [Int] -> [Int]
threeSums l@(_:x:xs) = zipWith3 (\x y z -> x + y + z) l (x:xs) xs
threeSums _          = error "list too short"
