module Day6 ( getFishies
            , solution1
            , solution2
            ) where
import           Data.Char   (isNumber)
import           Data.List
import           Debug.Trace

-- |very simple and idiomatic solution but really, really slow
solution1 :: [Int] -> Int
solution1 = length . flip (!!) 80 . iterate (>>= pass)
  where
    pass :: Int -> [Int]
    pass x = if x <= 0 then [6,8] else [x-1]


-- needed to do some optimizing, first one was to slow for the second task :D
solution2 :: [Int] -> Int
solution2 = sum . map fst . flip (!!) 256 . iterate pass . register
  where
    pass :: Fishies -> Fishies
    pass = clean 6 . (treat =<<)

    clean :: Int -> Fishies -> Fishies
    clean x fs = let (y,n) = partition ((==x) . snd) fs in if not $ null y then foldl1 ac y : n else n

    ac :: (Int, Int) -> (Int, Int) -> (Int, Int)
    ac (c1, d1) (c2, d2) = (c1+c2, d1)

    treat :: (Int,Int) -> [(Int,Int)]
    treat p = let (count, days) = p in
                  if days <= 0 then [(count, 8), (count, 6)] else [(count, days-1)]

    register :: [Int] -> Fishies
    register xs = (\i n -> ((length . filter (==n)) i, n)) xs <$> [1..6]

type Fishies = [(Int, Int)]

-- |parsing
getFishies :: String -> [Int]
getFishies [] = []
getFishies (',':xs) = getFishies xs
getFishies s = read (takeWhile isNumber s) : getFishies (dropWhile isNumber s)
