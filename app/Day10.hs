module Day10 ( solution1
             , solution2
             ) where

import           Control.Monad.State
import           Data.List           (elemIndex, sort)
import           Data.Maybe          (mapMaybe)

type Stack = ([Char] ,[Char])

type Checker = State Stack (Maybe Char)

-- second part should be easy

solution1 :: [String] -> Int
solution1 = sum . mapMaybe (fmap score . flip evalState ([],[]) . matchBracket)
  where
    score :: Char -> Int
    score c = case c of
                ')' -> 3
                ']' -> 57
                '}' -> 1197
                '>' -> 25137
                _   -> error "this shouldn't happen"

solution2 :: [String] -> Integer
solution2 = middle . sort . mapMaybe (fmap (score . complete) . validate . flip execState ([],[]) . matchBracket)
  where
    middle :: [a] -> a
    middle l = let mid = length l `div` 2 in l !! mid

    validate :: ([Char], [Char]) -> Maybe [Char]
    validate ([], stack) = Just stack
    validate _           = Nothing

    complete :: String -> String
    complete = fmap matching

    score :: String -> Integer
    score = foldl (\acc x -> acc*5 + pts x ) 0

    pts :: Char -> Integer
    pts c = case lookup c (zip closing [1..]) of
                Nothing -> error "this should not happen"
                Just a  -> a


matchBracket :: String -> Checker
matchBracket [] = do
  (failed, stack) <- get
  return (safeLast failed)
matchBracket (c:cs) = do
  (failed, stack) <- get
  if not $ null stack
     then
     case (head stack `elem` opening, c `elem` opening) of
       (True, True) -> put (failed,c:stack)
       (True, False) -> if head stack `matches` c then put (failed, tail stack) else put (c:failed, tail stack)
       (False, _) -> error "there shouldn't be closing brackets on the stack"
     else put ([], [c])
  matchBracket cs

safeLast :: [a] -> Maybe a
safeLast l = if not $ null l then Just $ last l else Nothing

matches :: Char -> Char -> Bool
matches = (==) . matching

matching :: Char -> Char
matching c = case elemIndex c opening of
               Nothing -> error "unexpected token"
               Just i  -> closing !! i

opening, closing :: [Char]
opening = "([{<"
closing = ")]}>"

