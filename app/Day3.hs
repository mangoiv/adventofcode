module Day3 ( toBinary
            , solution1
            , solution2
            ) where

import           Debug.Trace

data Letter = Epsilon
            | Gamma

toBinary :: String -> [Int]
toBinary = fmap sb
  where
    sb '1' = 1
    sb '0' = 0
    sb _   = error "input invalid"

solution1 :: [[Int]] -> Int
solution1 = generalSolution greekLetter

solution2 :: [[Int]] -> Int
solution2 = generalSolution gasrating

generalSolution :: (Letter -> [[Int]] -> [Int]) -> [[Int]] -> Int
generalSolution f l = product $ toDecimal <$> [f Gamma l, f Epsilon l]

gasrating :: Letter -> [[Int]] -> [Int]
gasrating lt = go 0
             where
               go :: Int -> [[Int]] -> [Int]
               go pos [a] = a
               go pos ls  =
                 go (pos+1) (filterByBit (greekLetter lt ls !! pos , pos) ls)

filterByBit :: (Int, Int) -> [[Int]] -> [[Int]]
filterByBit (c, pos) =  filter (\r -> (r !! pos) == c )

greekLetter :: Letter -> [[Int]] -> [Int]
greekLetter f l = h f <$> foldl (zipWith (+)) (replicate ln 0) l
  where
    h Gamma x   =
      case compare (x,0) (length l `divMod` 2) of
        GT -> 1
        LT -> 0
        EQ -> 1
    h Epsilon x =
      case compare (x,0) (length l `divMod` 2) of
        GT -> 0
        LT -> 1
        EQ -> 0
    ln :: Int
    ln = length $ head l

toDecimal :: [Int] -> Int
toDecimal l = sum $ reverse $ zipWith (\b e -> b*2^e) (reverse l) [0..]
